  package com.example.crudlive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudliveApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudliveApplication.class, args);
	}

}
